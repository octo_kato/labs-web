/**
 * @typedef Teacher
 * @property {integer} id
 * @property {string} firstName
 * @property {string} lastName
 * @property {string} dataOfBirth
 * @property {string} photoUrl
 * @property {string} email
 * @property {number} couplesPerWeek
 * @property {string} phone
 */
class Teacher {
    constructor(model) {
        this.id = model.id;
        this.firstName = model.firstName;
        this.lastName = model.lastName;
        this.dataOfBirth = model.dataOfBirth;
        this.photoUrl = model.photoUrl;
        this.email = model.email;
        this.couplesPerWeek = model.couplesPerWeek;
        this.phone = model.phone; 
    }
};
 
module.exports = Teacher;