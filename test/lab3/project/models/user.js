/**
 * @typedef User
 * @property {integer} id
 * @property {string} login.required - unique username
 * @property {string} fullname - some description here
 * @property {number} role
 * @property {string} registeredAt
 * @property {string} avaUrl
 * @property {boolean} isEnabled
 */
class User {
    constructor(model) {
        this.id = model.id;  
        this.login = model.login; 
        this.fullname = model.fullname; 
        this.role = model.role;
        this.avaUrl = model.avaUrl;
        this.isEnebled = model.isEnebled;
    }
 };
 
 module.exports = User;
 