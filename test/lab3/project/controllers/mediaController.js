const mediaRepositpory = require('./../repositories/mediaRepository')
const mediaProperty = Symbol('media')
const mediaRepo = new mediaRepositpory('./data/media.json');
const fs = require('fs');

module.exports = {
    getMediaById(req, res) {
        const media = mediaRepo.getMediaById(parseInt(req.params.id));
        if (media) {
            req[mediaProperty] = fs.readFileSync(__dirname + '/../data/media/' + media.filename);
            res.send(Buffer.from(req[mediaProperty]));
        } 
        else res.sendStatus(404);
    },
    addMedia(req, res) {
        let name = req.files['media'].name.split('.');
        let media = {};
        media.filename = 'teacher' + mediaRepo.storage.nextId + '.'+name[1];
        console.log(media.filename);
        fs.writeFileSync('./data/media/' + media.filename, req.files['media'].data);
        id = mediaRepo.addMedia(media);
        res.status(201).send(id.toString());
    }
};
