const teacherRepository = require('./../repositories/teacherRepository');

const teacherRepo = new teacherRepository('./data/teachers.json');

const teacherProperty = Symbol('teacher');
const fs = require('fs');

module.exports = {
    getTeachers(req, res) {
        const page = req.query.page;
        const per_page = req.query.per_page;
        if (page && per_page) {
            res.send(teacherRepo.getTeachers().slice((page - 1) * per_page, page * per_page));
        }
        else {
            req[teacherProperty] = teacherRepo.getTeachers();
            res.send(req[teacherProperty]);
        }
        res.end();
    },
    getTeacherById(req, res, next) {
        const teacher = teacherRepo.getTeacherById(parseInt(req.params.id));
        if(teacher){
            req[teacherProperty] = teacher;
            res.send(req[teacherProperty]);
        }
        else res.sendStatus(404);    
    },
    deleteTeacherById(req, res, next) {
        teacherRepo.deleteTeacher(parseInt(req.params.id));
        res.send(req[teacherProperty]);
    },
    addTeacher(req, res, next) {
        fs.writeFileSync('./returned/teacher.json', req.files['teacher'].data);
        const jsonText = fs.readFileSync('./returned/teacher.json');
        const jsonArray = JSON.parse(jsonText);
        console.log(jsonArray);
        if(!('firstName' in jsonArray) || !('lastName'in jsonArray)|| !('dataOfBirth'in jsonArray)||
        !('photoUrl' in jsonArray)||!('email'in jsonArray)||!('phone'in jsonArray)||!('couplesPerWeek'in jsonArray))
        {
            res.sendStatus(400);
        }
        else{
            id = teacherRepo.addTeacher(jsonArray);
            jsonArray.id = id;
            req[teacherProperty] = jsonArray;
            res.statusCode = 201;
            res.setHeader("Location","http://localhost:3000/api/teachers/"+id);
            res.send(req[teacherProperty])
        }
    },
    updateTeacher(req, res, next) {
        const teacher = teacherRepo.getTeacherById(parseInt(req.params.id));
        fs.writeFileSync('./returned/teacher.json', req.files['teacher'].data);
        const jsonText = fs.readFileSync('./returned/teacher.json');
        const jsonArray = JSON.parse(jsonText);
        if('firstName' in jsonArray) teacher.firstName = jsonArray.firstName;
        if('lastName' in jsonArray) teacher.lastName = jsonArray.lastName;
        if('dataOfBirth' in jsonArray) teacher.dataOfBirth = jsonArray.dataOfBirth;
        if('photoUrl' in jsonArray) teacher.photoUrl = jsonArray.photoUrl;
        if('email' in jsonArray) teacher.email = jsonArray.email;
        if('phone' in jsonArray) teacher.phone = jsonArray.phone
        
        teacherRepo.updateTeacher(teacher);
        req[teacherProperty] = teacher;
        res.send(req[teacherProperty])
    },
    getTeacherHandler(req, res, next) {
        const teacher = teacherRepo.getTeacherById(parseInt(req.params.id));
        if (teacher) {
            req[teacherProperty] = teacher;
            next();
        }
        else res.sendStatus(404);
    }
};