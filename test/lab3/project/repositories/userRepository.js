const User = require('../models/user');
const JsonStorage = require('./jsonStorage');
 
class UserRepository {
    
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }

    getUsers() { 
        const items = this.storage.readItems();
        const users = [];
        for (const item of items) {
            users.push(new User(item));
        }
        return users;
    }
    
    getUserById(id) {
        const items = this.storage.readItems();
        for (const item of items) {
            if (item.id === id) {
                return new User(item);
            }
        }
        return null;
    }
};
 
module.exports = UserRepository;
