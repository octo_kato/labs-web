const router = require('express').Router();

const userRouter = require('./users');
const teacherRouter = require('./teachers');
const mediaRouter = require('./media');

router.use('/users', userRouter);
router.use('/teachers', teacherRouter);
router.use('/media', mediaRouter);
module.exports = router;
