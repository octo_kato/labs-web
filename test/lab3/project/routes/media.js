const express = require('express');
const mediaController = require('./../controllers/mediaController');
const Router = express.Router();

/**
* return media by id
* @route GET /api/media/{id}
* @group Media - upload and get images
* @param {integer} id.path.required - id of the media - eg: 1
* @returns {file} 200 - file with media
* @returns {Error} 404 - Media not found
*/
Router.get('/:id', mediaController.getMediaById)
/**
* return media file
* @route POST /api/media
* @group Media - upload and get images
* @param {file} media.formData.required - uploaded image
* @returns {integer} 200 - added image id
*/
    .post('/', mediaController.addMedia);

module.exports = Router;