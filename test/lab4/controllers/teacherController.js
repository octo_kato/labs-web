const teacherRepository = require('./../repositories/teacherRepository');
const teacherRepo = new teacherRepository('mongodb://localhost:27017/webprogbase');
const MediaRepository = require('./../repositories/mediaRepository');
const mediaRepo = new MediaRepository('./data/media.json');
const path = require('path');

const cons = require('consolidate');
const fs = require('fs').promises;
const Teacher = require('../models/teacher');

module.exports = {
    async getTeachers(req, res) {
        try {
            const result = await Promise.all ([
                teacherRepo.getTeacherPaginated(Number(req.query.page), Number(req.query.per_page), req.query.name),
                teacherRepo.getPagesNumber(Number(req.query.page), Number(req.query.per_page), req.query.name)
            ]);
            const teachers = result[0];
            const pagesNumber = result[1];
            let page = req.query.page;
            let name = req.query.name;
            if (!page) page = 1;
            else page = Number(page);
            const pages = { currentPage: Number(page) };

            if (page != 1) pages.prevPage = page - 1;
            if (page != pagesNumber) pages.nextPage = page + 1; 
            if (name) pages.namePage = name;

            if (teachers) 
                res.status(200).render('teachers', {teachers: teachers, pagesNumber: pagesNumber, pages: pages, teacherDisabled: "disabled"});
            else 
                res.status(404).send({teachers: null, message: "Not found."});

        } catch (err) {
            console.log(err.message);
            res.status(500).send({teachers: null, message: 'Server error.'});
        }
    },
    async getTeacherById(req, res, next) {
        try {
            const teacher = await teacherRepo.getTeacherById(req.params.id);
            if (teacher) res.status(200).render('teacher', {teacher: teacher});
            else res.status(404).send({teacher: null, message: "Not found."});
        } catch (err) {
            console.log(err.message);
            res.status(500).send({teacher: null, message: 'Server error.'});
        }
    },
    async deleteTeacherById(req, res, next) {
        await teacherRepo.deleteTeacher(req.params.id);
        res.redirect('/teachers');
    },
    async addTeacher(req, res, next) {
        console.log(req.files);
        const fileFormat = req.files['photoUrl'].mimetype.split('/')[1];
        fs.writeFile(path.resolve(__dirname, '../data/media/teacher' + (await (mediaRepo.storage).nextId()) + '.' + fileFormat), req.files['photoUrl'].data, (err) => {
            if (err) {

                console.log("Can't load this photo.");

            }
        })
        const newTeacher = new Teacher({"id": 0, 
        "firstName": " ",
        "lastName": " ",
        "dataOfBirth": " ",
        "photoUrl": " ",
        "email": " ",
        "couplesPerWeek": 1,
        "phone": " "});
        newTeacher.id = 0;
        newTeacher.firstName = req.body.firstName;
        newTeacher.lastName = req.body.lastName;
        newTeacher.dataOfBirth = req.body.dataOfBirth;
        newTeacher.photoUrl = '/media/teacher' + await mediaRepo.storage.nextId() + '.' + fileFormat;
        await mediaRepo.storage.incrementNextId();
        newTeacher.email = req.body.email;
        newTeacher.couplesPerWeek = parseInt(req.body.couplesPerWeek);
        newTeacher.phone = req.body.phone; 
        const newId = await teacherRepo.addTeacher(newTeacher);
        console.log(newId);
        res.redirect('/teachers/' + newId);
    },
    async updateTeacher(req, res, next) {
        try {

            if (!req.body.firstName || !req.body.lastName || !req.body.dataOfBirth || 
                !req.body.photoUrl || !req.body.email || !req.body.phone) 
                res.status(400).send({message: 'Bad request'});
            teacher = await teacherRepo.updateTeacher(req.body);
            if (teacher) {
                console.log(teacher);
                res.status(200).send({teacher: teacher, message: "Success"});
            }
            else 
                res.status(404).send({teacher: null, message: "Not found"});

        } catch (err) {
            console.log(err.message);
            res.status(500).send({teacher: null, message: 'Server error.'});
        }
    },
    async getAllTeachers(req, res, next) {
        try{
            const teachers = await teacherRepo.getTeachers();
            if (teachers) res.status(200).render('teachers', {teachers: teachers, teacherDisabled: "disabled"});
            else res.status(404).send({teachers: null, message: "Not found."});
        } catch (err) {
            console.log(err.message);
            res.status(500).send({teachers: null, message: 'Server error.'});
        }

    }
};