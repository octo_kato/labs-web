const Teacher = require('../models/teacher');
const TeacherSchema = require('../schemas/teacherSchema');
const JsonStorage = require('./jsonStorage');
const mongoose = require('mongoose');
 
class TeacherRepository {
 
    constructor(dbUrl) {
        const teacherModel = mongoose.model("teacher", TeacherSchema);
        this.storage = new JsonStorage(dbUrl, teacherModel);
    }
    async addTeacher(teacherModel){
        // const result = await Promise.all([
        //     this.storage.getItems(),
        //     this.storage.nextId()
        // ]);
        // const teachers = result[0].items;
        // teacherModel.id = result[1];
        // teachers.push(teacherModel);
        // await this.storage.incrementNextId();
        // await this.storage.writeItems(teachers);
        return teacherModel.id;
    }
    async getTeachers(){
        await this.storage.connect();
        const res = await this.storage.getItems();
        console.log(await this.storage.getCount());
        await this.storage.disconnect();
        return res;
    }
    async getTeacherById(teacherId) {
        await this.storage.connect();
        const res = await this.storage.getById(teacherId);
        await this.storage.disconnect();
        return res;
    }
    async getPagesNumber(page, per_page, name) {
        const page_size = 3;
        const maxPageSize = 3;
        if (per_page) {
            if (per_page > maxPageSize) {
                console.log("Error");
                return undefined;
            }
        }
        else per_page = page_size;

        if (!page) page = 1;

        const teachers = await this.getTeachers();
        const teachersNumber = Number(teachers.length)
        const offset = per_page * (page - 1);

        if (teachersNumber <= offset) {
            console.log("Error");
            return undefined;
        }

        let resTeachers = [];
        let tempTeachersLen = 0;

        if (name) {
            for (let i = 0; i < teachers.length; i++) {
                if (teachers[i].lastName.includes(name)) resTeachers.push(teachers[i]);
            }
            tempTeachersLen = resTeachers.length;
            resTeachers = resTeachers.slice(offset, offset + per_page);
        }
        const currentTeachers = teachers.slice(offset, offset + per_page);
        let pagesNumber = 0;

        if ((teachersNumber / per_page) - Math.trunc(teachersNumber / per_page) != 0) 
            pagesNumber = Math.trunc(teachersNumber / per_page) + 1;
        else
            pagesNumber = Math.trunc(teachersNumber / per_page);

        if (name) {
            if ((tempTeachersLen / per_page) - Math.trunc(tempTeachersLen / per_page) != 0) 
                pagesNumber = Math.trunc(tempTeachersLen / per_page) + 1;
            else 
                pagesNumber = Math.trunc(tempTeachersLen / per_page);

            if (pagesNumber == 0) pagesNumber = 1;

            return pagesNumber;
        }

        if (pagesNumber == 0) pagesNumber = 1;
        
        return pagesNumber;
    }

    async getTeacherPaginated(page, per_page, name) {
        const page_size = 3;
        const maxPageSize = 3;
        if (per_page) {
            if (per_page > maxPageSize) {
                console.log("Error");
                return undefined;
            }
        }
        else per_page = page_size;

        if (!page) page = 1;

        const teachers = await this.getTeachers();
        const teachersNumber = await this.storage.getCount();
        const offset = per_page * (page - 1);

        if (teachersNumber <= offset) {
            console.log("Error");
            return undefined;
        }

        let resTeachers = [];

        if (name) {
            for (let i = 0; i < teachers.length; i++) {
                if (teachers[i].lastName.includes(name)) 
                    resTeachers.push(teachers[i]);
            }
            resTeachers = resTeachers.slice(offset, offset + per_page);
        }
        const currentTeachers = teachers.slice(offset, offset + per_page);
        if (name) return resTeachers;
        return currentTeachers;
    }
    async updateTeacher(teacherModel){
        const teachers = await this.storage.readItems().items;

        if (teacherModel.id < await this.storage.readItems().nextId()) {
            let check = false;
            let index = 0;
            console.log(teacherModel.id);

            for (const item of teachers) {
                if (parseInt(item.id) === parseInt(teacherModel.id)) {
                    teacherModel.id = parseInt(teacherModel.id);
                    teacherModel.couplesPerWeek = parseInt(teacherModel.couplesPerWeek);

                    console.log("Teacher has been updated successfully.");
                    check = true;
                    const updatedTeacher = new Teacher(teacherModel);
                    teachers[index] = updatedTeacher;
                    break;
                }
                index++;
                console.log(index);
            }
            if (check === false) console.log("Teacher with such id doesn't exist.");
        }
        else console.log("Teacher with such id doesn't exist");
        await this.storage.writeItems(teachers);
        return teacherModel;
    }
    async deleteTeacher(teacherId){

        // const teachers = (await this.storage.readItems()).items;
        // const teacher = await this.getTeacherById(teacherId);
        // if (teacher) {
        //     const index = teachers.findIndex((t) => {
        //         return t.id === teacher.id;
        //     });
        //     teachers.splice(index, 1);
        //     await this.storage.writeItems(teachers);
        //     console.log("Teacher has been deleted successfully.");
        //     return teacher;
        // }
        // else console.log("Teacher with such id doesn't exist");
        // return undefined;
    }
};
 
module.exports = TeacherRepository;
