const User = require('../models/user');
const JsonStorage = require('./jsonStorage');
const UserSchema = require('../schemas/userSchema'); 
const mongoose = require('mongoose');

class UserRepository {
    
    constructor(dbUrl) {
        const userModel = mongoose.model("user", UserSchema);
        this.storage = new JsonStorage(dbUrl, userModel);
    }
    async getUsers() { 
        await this.storage.connect();
        const res = await this.storage.getItems();
        console.log(await this.storage.getCount());
        await this.storage.disconnect();
        return res;
    }
    async getUserById(userId) {
        this.storage.connect();
        const res = await this.storage.getById(userId);
        this.storage.disconnect();
        return res;
    }
    async getUsersPaginated(page, per_page) {
        const page_size = 3;
        const maxPageSize = 5;
        if (per_page) {
            if (per_page > maxPageSize) {
                console.log("Error");
                return null;
            }
        }
        else per_page = page_size;

        if (!page) page = 1;

        const users = await this.getUsers();
        const usersNumber = Number(users.length);
        const offset = per_page * (page - 1);

        if (usersNumber < offset) {
            console.log("Error");
            return null;
        }
        const currentUsers = users.slice(offset, offset + per_page);
        
        return currentUsers;
    }
    async addUser(userModel) {
        // const items = await this.getUsers();
        // await this.storage.incrementNextId();
        // const newUser = new User(userModel);
        // items.push(newUser);
        // await this.storage.writeItems(items);
        // return await (await this.storage.readItems()).nextId();
    }
    async updateUser(userModel) {
        // const items = await this.getUsers();
        // let check = false;

        // for (const item of items) {
        //     if (item.id === userModel.id) {
        //         check = true;
        //         items[item.id - 1] = userModel;
        //         break;
        //     }
        // }

        // if (check === false) console.log("User with such id doesn't exist");
        // await this.storage.writeItems(items);
    }
    async deleteUser(userId) {
    //     const items = await this.getUsers();
    //     let check = false;

    //     for (const item of items) {
    //         if (item.id === userId) {
    //             check = true;
    //             items.splice(item.id - 1, 1);
    //             break;
    //         }
    //     }

    //     if (check === false) console.log("User with such id doesn't exist");
    //     await this.storage.writeItems(items);
    }
};
 
module.exports = UserRepository;
