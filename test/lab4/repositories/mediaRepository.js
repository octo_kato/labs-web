const JsonStorage = require('./jsonStorage');
const fs = require('fs').promises;

class MediaRepository {
 
    constructor(filePath) {
        this.path = (filePath.split('.'))[0];
        this.storage = new JsonStorage(filePath);
    }
    async getMediaPath(id) {
        for (const item of this.allFileFormats()) {
            const fullPath = this.path + '/teacher' + String(id) + '.' + item;
            if (fs.exists(fullPath)) return fullPath;
        }
        return undefined;
    }
    async getCurrentId() {
        return (await this.storage.nextId()) - 1;
    }
    async allFileFormats() {
        return (await this.storage.readItems()).fileFormats;
    }
};
 
module.exports = MediaRepository;