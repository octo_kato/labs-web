//const url = 'mongodb://localhost:27017/';  // Connection URL
const mongoose = require('mongoose');

const conOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
};  // Connection options

class JsonStorage {

    constructor(dbUrl, model) {
        this.dbUrl = dbUrl;
        this.model = model;
    }

    async connect(){
        this.client = await mongoose.connect(this.dbUrl, conOptions);
        console.log(`Successfully connected to database server at ${this.dbUrl}`);
    }

    async disconnect(){
        await this.client.disconnect();
        console.log('Closed conection');
    }

    async update(item){
        await this.model.findByIdAndUpdate(item._id, item);
    }

    async insert (item) {
        const result = await this.model(item).save();
        console.log("in add");
    }

    async getItems(){
        return await this.model.find();
    }

    async delete(itemId){
        const status = await this.model.findByIdAndDelete(itemId);
        console.log("Removed %d documents", status.result.n);
    }
    
    async getById(id_item){
        const res = await this.model.findById(id_item);
        return res;
    }

    async getCount(){
        return await this.model.estimatedDocumentCount();
    }

    async getPaginatedItems(perPage, count){
        return await this.model
            .find().
            limit(count).
            skip(perPage * (count - 1));
    }
};

module.exports = JsonStorage;