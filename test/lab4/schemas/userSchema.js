const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.ObjectId;

let userSchema = new Schema({
    _id:{ type: ObjectId },
    login: {
      type: String,
      required: true,
      minlength: 1,
      maxlength: 20,
      unique: true
    },
    role: {
      type: Number,
      required: true,
      min: 0,
      max: 1,
      unique: false
    },
    fullname: {
      type: String,
      required: true,
      minlength: 1,
      maxlength: 30,
      unique: false
    },
    registeredAt: {
      type: Date,
      required: true,
      unique: false
    },
    isDisabled: {
      type: Boolean,
      required: true,
      unique: false
    },
    avaUrl: {
      type: String,
      required: true,
      unique: false
    },
    bio: {
      type: String,
      required: true,
      minlength: 1,
      maxlength: 1000,
      unique: false
    },
    password: {
      type: String,
      required: true,
      unique: false
    }
  }, {collection: 'teachers'})

module.exports = userSchema;
