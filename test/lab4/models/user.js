// const { ObjectId } = require('bson');
// const userScheme = require('../schemas/userSchema');

class User {
    constructor(model) {
        this._id = model._id;
        this.login = model.login; 
        this.fullname = model.fullname;  
        this.role = model.role;
        this.registeredAt = model.registeredAt;
        this.avaUrl = model.avaUrl;
        this.isEnebled = model.isEnebled;
        // this.isDisabled = model.isDisabled;
        // this.password = model.password;
        // this.bio = model.bio
    }

 };
 
 module.exports = User;
 