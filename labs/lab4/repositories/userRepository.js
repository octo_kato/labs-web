const User = require('../models/user');
const JsonStorage = require('./jsonStorage');
const UserSchema = require('../schemas/userSchema'); 

class UserRepository {
    
    constructor() {
        this.storage = new JsonStorage(UserSchema);
    }
    async getUsers() { 
        return await this.storage.getItems();
    }
    async getUserById(userId) {
        return await this.storage.getById(userId);
    }
    async getUsersPaginated(page, per_page) {
        const page_size = 3;
        const maxPageSize = 5;
        if (per_page) {
            if (per_page > maxPageSize) {
                console.log("Error");
                return null;
            }
        }
        else per_page = page_size;

        if (!page) page = 1;

        const users = await this.getUsers();
        const usersNumber = Number(users.length);
        const offset = per_page * (page - 1);

        if (usersNumber < offset) {
            console.log("Error");
            return null;
        }
        const currentUsers = users.slice(offset, offset + per_page);
        
        return currentUsers;
    }
    async addUser(userModel) {

    }
    async updateUser(userModel) {

    }
    async deleteUser(userId) {

    }
};
 
module.exports = UserRepository;
