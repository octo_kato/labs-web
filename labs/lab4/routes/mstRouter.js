const router = require('express').Router();

const userRouter = require('./users');
const teacherRouter = require('./teachers');
const universityRouter = require('./universities');

router.use('/users', userRouter);
router.use('/teachers', teacherRouter);
router.use('/universities', universityRouter);

module.exports = router;
