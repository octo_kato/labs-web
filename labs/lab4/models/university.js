class University {
    constructor(model) {
        this._id = model._id;
        this.name = model.name;
        this.photoUrl = model.photoUrl;
        this.country = model.country;
        this.town = model.town;
        this.corps = model.corps;
        this.site = model.site;
    }
};
 
module.exports = University;