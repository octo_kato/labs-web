// let mongoose = require('mongoose');

// let scheduleSchema = new mongoose.Schema({
//   subjectName: {
//     type: String,
//     required: true,
//     unique: false
//   },
//   date

//   creationDate: {
//       type: Date,
//       required: true,
//       unique: false
//   },
//   photoUrl: {
//       type:String,
//       required: true,
//       unique: false
//   },
//   teachers: [{
//       type : mongoose.Schema.Types.ObjectId, ref: ''
//   }]
// })

// module.exports = scheduleSchema;
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.ObjectId;

let universitySchema = new Schema({
    // _id:{ type: ObjectId },
    name: {
      type: String,
      required: true,
      minlength: 1,
      maxlength: 30,
      unique: false
    },
    photoUrl: {
        type: String,
        required: true,
        unique: false
    },
    country: {
      type: String,
      required: true,
      minlength: 1,
      maxlength: 30,
      unique: false
    },
    town: {
        type: String,
        required: true,
        minlength: 1,
        maxlength: 30,
        unique: false
    },
    corps: {
        type: Number,
        required: true,
        min: 0,
        unique: false
    },
    site: {
      type: String,
      required: true,
      minlength: 1,
      maxlength: 30,
      unique: false
    },
    userId: {
      type: ObjectId,
      ref: 'user'
    }
}, {collection: 'universities'})

module.exports =   mongoose.model("university", universitySchema);
