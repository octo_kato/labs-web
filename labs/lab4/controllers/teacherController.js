const teacherRepository = require('./../repositories/teacherRepository');
const mediaRepository = require('./../repositories/mediaRepository');
const teacherRepo = new teacherRepository();
const universityRepository = require('./../repositories/universityRepository');
const universityRepo = new universityRepository();
const mediaRepo = new mediaRepository();

module.exports = {
    
    async getTeachers(req, res, next) {
        try {
            const teachers = await teacherRepo.getTeacherPaginated(Number(req.query.page), Number(req.query.per_page), req.query.name);
            const pagesNumber = await teacherRepo.getPagesNumber(Number(req.query.page), Number(req.query.per_page), req.query.name);
            //
            let page = req.query.page;
            let name = req.query.name;
            if (!page) page = 1;
            else page = Number(page);
            const pages = { currentPage: Number(page) };

            if (page != 1) pages.prevPage = page - 1;
            if (page != pagesNumber) pages.nextPage = page + 1; 
            if (name) pages.namePage = name;

            if (teachers) 
                res.status(200).render('teachers', {teachers: teachers, pagesNumber: pagesNumber, pages: pages, teacherDisabled: "disabled"});
            else 
                res.status(404).send({teachers: null, message: "Not found."});

        } catch (err) {
            return await next(err);
        }
    },
    async getTeacherById(req, res, next) {
        try {
            console.log("in get t by id");
            const teacher = await teacherRepo.getTeacherById(req.params.id);
            if (teacher) res.status(200).render('teacher', {teacher: teacher});
            else res.status(404).send({teacher: null, message: "Not found."});
        } catch (err) {
            return await next(err);
        }
    },
    async deleteTeacherById(req, res, next) {
        try{
            console.log("in delete t by id");
            await teacherRepo.deleteTeacher(req.params.id);
            res.redirect('/teachers');
        }catch(err){
            return await next(err);
        }
    },
    async addTeacher(req, res, next){
        try {
            console.log(req.files['photoUrl']);
            const photo = await mediaRepo.uploadRaw(req.files['photoUrl'].data);
            const newTeacher = { 
            "firstName": req.body.firstName,
            "lastName": req.body.lastName,
            "dataOfBirth": req.body.dataOfBirth,
            "photoUrl": photo.url,
            "email": req.body.email,
            "couplesPerWeek": parseInt(req.body.couplesPerWeek),
            "phone": req.body.phone,
            "universityId": req.body.selectUniversity
             };
             
            console.log(newTeacher);
            const newId = await teacherRepo.addTeacher(newTeacher);
            console.log(newId);
            res.redirect('/teachers/' + newId);
        }catch(err){
            return await next(err);
        }
    },
    async getUniversitiesForNew(req, res, next) {
        try {
            const universities = await universityRepo.getUniversities(); 
            if (universities) {
                res.status(200).render('new', {universities: universities, universityDisabled: "disabled"});
            }
            else 
                res.status(404).send({universities: null, message: "Not found."});
        }catch(err){
            return await next(err);
        }
    },
    async updateTeacher(req, res, next) {
        try {

            if (!req.body.firstName || !req.body.lastName || !req.body.dataOfBirth || 
                !req.body.photoUrl || !req.body.email || !req.body.phone) 
                res.status(400).send({message: 'Bad request'});
            teacher = await teacherRepo.updateTeacher(req.body);
            if (teacher) {
                console.log(teacher);
                res.status(200).send({teacher: teacher, message: "Success"});
            }
            else 
                res.status(404).send({teacher: null, message: "Not found"});

        } catch (err) {
            return await next(err);
        }
    },
    async getAllTeachers(req, res, next) {
        try{
            const teachers = await teacherRepo.getTeachers();
            if (teachers) res.status(200).render('teachers', {teachers: teachers, teacherDisabled: "disabled"});
            else res.status(404).send({teachers: null, message: "Not found."});
        } catch (err) {
            return await next(err);
        }

    }, 
    async anyError(err, req, res, next){
        console.error(err.stack)
        res.status(500).send('Something broke!')
    }
};