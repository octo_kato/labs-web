// const teacherScheme = require('../schemas/teacherSchema');

class Teacher {
    constructor(model) {
        this._id = model._id;
        this.firstName = model.firstName;
        this.lastName = model.lastName;
        this.dataOfBirth = model.dataOfBirth;
        this.photoUrl = model.photoUrl;
        this.email = model.email;
        this.couplesPerWeek = model.couplesPerWeek;
        this.phone = model.phone; 
    }
    // createNew(){
    //     this.id = 0;
    //     this.firstName = " ";
    //     this.lastName = " ";
    //     this.dataOfBirth = " ";
    //     this.photoUrl = " ";
    //     this.email = " ";
    //     this.couplesPerWeek = 0;
    //     this.phone = " ";
    // }
};
 
module.exports = Teacher;