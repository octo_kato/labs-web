const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.ObjectId;

let teacherSchema = new Schema({
    // _id:{ type: ObjectId },
    firstName: {
      type: String,
      required: true,
      minlength: 1,
      maxlength: 30,
      unique: false
    },
    lastName: {
      type: String,
      required: true,
      minlength: 1,
      maxlength: 30,
      unique: false
    },
    dataOfBirth: {
      type: Date,
      required: true,
      unique: false
    },
    photoUrl: {
      type: String,
      required: true,
      unique: false
    },
    email: {
      type: String,
      required: true,
      minlength: 1,
      maxlength: 30,
      unique: true
    },
    phone: {
      type: String,
      required: true,
      minlength: 1,
      maxlength: 30,
      unique: false
    },
    couplesPerWeek: {
      type: Number,
      required: true,
      min: 0,
      unique: false
    },
    universityId: {
      type: ObjectId,
      ref: 'university'
    }
}, {collection: 'teachers'})

module.exports =  mongoose.model("teacher", teacherSchema);
