class JsonStorage {

    constructor(model) {
        this.model = model;
    }

    async update(item){
        await this.model.findByIdAndUpdate(item._id, item);
    }

    async insert (item) {
        const result = await this.model(item).save();
        console.log("in add");
        return result._id;
    }

    async getItems(){
        return await this.model.find().populate("universityId").populate("userId");
        //.populate("universityId").populate("user");
    }

    async delete(itemId){
        const status = await this.model.findByIdAndRemove(itemId);
        return status;
    }
    
    async getById(id_item){
        return await this.model.findById(id_item).populate("universityId").populate("userId");
        //.populate("universityId");
    }

    async getCount(){
        const res = await this.model.estimatedDocumentCount();
        return res;
    }

    async getPaginatedItems(perPage, count){
        return await this.model
            .find().
            limit(count).
            skip(perPage * (count - 1));
    }
};

module.exports = JsonStorage;