const University = require('../models/university');
const UniversitySchema = require('../schemas/universitySchema');
const JsonStorage = require('./jsonStorage');
 
class UniversityRepository {
 
    constructor() {
        this.storage = new JsonStorage(UniversitySchema);
    }
    async addUniversity(universityModel){
        const res = await this.storage.insert(universityModel);
        console.log(res);
        return res;
    }
    async getUniversities(){
        return await this.storage.getItems();
    }
    async getUniversityById(universityId) {
        return await this.storage.getById(universityId);
    }
    async getPagesNumber(page, per_page, name) {
        const page_size = 3;
        const maxPageSize = 3;
        if (per_page) {
            if (per_page > maxPageSize) {
                console.log("Error");
                return undefined;
            }
        }
        else per_page = page_size;

        if (!page) page = 1;

        const universities = await this.getUniversities();
        const universitiesNumber = Number(universities.length)
        const offset = per_page * (page - 1);

        if (universitiesNumber <= offset) {
            console.log("Error");
            return undefined;
        }

        let resUniversities = [];
        let tempUniversitiesLen = 0;

        if (name) {
            for (let i = 0; i < universities.length; i++) {
                if (universities[i].name.includes(name)) resUniversities.push(universities[i]);
            }
            tempUniversitiesLen = resUniversities.length;
            resUniversities = resUniversities.slice(offset, offset + per_page);
        }
        const currentUniversities = universities.slice(offset, offset + per_page);
        let pagesNumber = 0;

        if ((universitiesNumber / per_page) - Math.trunc(universitiesNumber / per_page) != 0) 
            pagesNumber = Math.trunc(universitiesNumber / per_page) + 1;
        else
            pagesNumber = Math.trunc(universitiesNumber / per_page);

        if (name) {
            if ((tempUniversitiesLen / per_page) - Math.trunc(tempUniversitiesLen / per_page) != 0) 
                pagesNumber = Math.trunc(tempUniversitiesLen / per_page) + 1;
            else 
                pagesNumber = Math.trunc(tempUniversitiesLen / per_page);

            if (pagesNumber == 0) pagesNumber = 1;

            return pagesNumber;
        }

        if (pagesNumber == 0) pagesNumber = 1;
        
        return pagesNumber;
    }

    async getUniversitiesPaginated(page, per_page, name) {
        const page_size = 3;
        const maxPageSize = 3;
        if (per_page) {
            if (per_page > maxPageSize) {
                console.log("Error");
                return undefined;
            }
        }
        else per_page = page_size;

        if (!page) page = 1;

        const universities = await this.getUniversities();
        const universitiesNumber = Number(await this.storage.getCount());
        const offset = per_page * (page - 1);

        if (universitiesNumber <= offset) {
            console.log("Error");
            return undefined;
        }

        let resUniversities = [];

        if (name) {
            for (let i = 0; i < universities.length; i++) {
                if (universities[i].name.includes(name)) 
                    resUniversities.push(universities[i]);
            }
            resUniversities = resUniversities.slice(offset, offset + per_page);
        }
        const currentUniversities = universities.slice(offset, offset + per_page);
        if (name) return resUniversities;
        return currentUniversities;
    }
    async updateUniversity(universityModel){
    }
    async deleteUniversity(universityId){
        // const res = await this.storage.delete(universityId);
        console.log(await this.storage.delete(universityId));
    }
};
 
module.exports = UniversityRepository;
