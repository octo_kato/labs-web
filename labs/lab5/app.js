const express = require('express');
const path = require('path');
const mustache = require('mustache-express');
const body_parser = require('body-parser')
const busboyBodyParser = require('busboy-body-parser');
const morgan = require('morgan');
const MongoClient  = require('mongoose');

const config = require('./config');
var app = express();

const viewsDir = path.join(__dirname, 'views');
app.engine('mst', mustache());
app.set('view engine', 'mst');
app.set('views', viewsDir);

app.engine("mst", mustache(path.join(viewsDir, "partials")));

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'data')));

const mstRouter = require('./routes/mstRouter');

app.use(morgan('dev'));
app.use(body_parser.json());
app.use(body_parser.urlencoded({ extended: false }));
app.use(busboyBodyParser());

const expressSwaggerGenerator = require('express-swagger-generator');
const cons = require('consolidate');
const expressSwagger = expressSwaggerGenerator(app);
 
const options = {
    swaggerDefinition: {
        info: {
            description: '(-_-)',
            title: 'LAB4',
            version: '1.0.0',
        },
        host: 'localhost:3000',
        produces: [ "application/json" ],
    },
    basedir: __dirname,
    files: ['./routes/**/*.js', './models/**/*.js'],
};
expressSwagger(options);

app.get('/', function(req, res) {
  res.render('index', {disabled: "disabled"});
});

app.get('/about', function(req, res) {
  res.render('about', {aboutDisabled: "disabled"});
});

app.use('', mstRouter);
app.use((req, res, next) => {
    res.status(400).send({ message: "Error in route."});
});

MongoClient.set('useCreateIndex', true);
const conOptions = { 
  useNewUrlParser: true, 
  useUnifiedTopology: true, 
  useFindAndModify: false };  // Connection options

// const port = process.env.PORT||3000;

const {db} = config.dev;

const connectionString = `mongodb+srv://${db.host}:${db.port}/${db.name}` ;
// console.log(connectionString);
// const connectionString = `mongodb://${db.host}:${db.port}/${db.name}`;

MongoClient.connect(connectionString, conOptions)
  .then((client) => {
      console.log(`Successfully connected to database server at ${connectionString}`);
  })
  .catch(err => {
      console.log(err);
  });

// console.log(MongoClient.connection.readyState);
// console.log(MongoClient.connection);

module.exports = app;