var express = require('express');
var router = express.Router();
const userController = require('../controllers/userController');

router.get('/', userController.getAllUsers, userController.anyError);
router.get('/:id', userController.getUserById, userController.anyError);

module.exports = router;
