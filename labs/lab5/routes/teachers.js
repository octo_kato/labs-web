const Router = require('express').Router();
const teacherController = require('./../controllers/teacherController');

Router.get("/new", teacherController.getUniversitiesForNew, teacherController.anyError);
Router.get("/:id", teacherController.getTeacherById, teacherController.anyError);
Router.post("/:id", teacherController.deleteTeacherById, teacherController.anyError);
Router.get("/", teacherController.getTeachers, teacherController.anyError);
Router.post("/", teacherController.addTeacher, teacherController.anyError);

module.exports = Router;