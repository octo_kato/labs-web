const Router = require('express').Router();
const universityController = require('../controllers/universityController');
const userController = require('../controllers/userController');

Router.get("/new",universityController.getUsersForNew, universityController.anyError);
Router.get("/:id", universityController.getUniversityById, universityController.anyError);
Router.post("/:id", universityController.deleteUniversityById, universityController.anyError);
Router.get("/", universityController.getUniversities, universityController.anyError);
Router.post("/", universityController.addUniversity, universityController.anyError);

module.exports = Router;