const universityRepository = require('../repositories/universityRepository');
const universityRepo = new universityRepository('mongodb://localhost:27017/webprogbase');
const userRepository = require('../repositories/userRepository');
const userRepo = new userRepository('mongodb://localhost:27017/webprogbase');
const MediaRepository = require('../repositories/mediaRepository');
const mediaRepo = new MediaRepository();
notifyAll = require('../bin/www');

const universityProperty = Symbol("university");

module.exports = {
    async getUniversities(req, res, next) {
        try {
            console.log(req.headers.onpage)
            if (req.headers.onpage) {
                const universities = await universityRepo.getUniversitiesPaginated(Number(req.query.page), Number(req.query.page_size), req.query.name)
                const pagesNumber = await universityRepo.getPagesNumber(Number(req.query.page), Number(req.query.page_size), req.query.name);
                //
                let page = req.query.page;
                let name = req.query.name;
                if (!page) page = 1;
                else page = Number(page);
                console.log(req.query.page, req.query.page_size, req.query.name);

                data = {}
                data.universities = universities
                data.pagesNumber = pagesNumber
                data.currentPage = page
                req[universityProperty] = data;
                res.send(req[universityProperty]);
                
            }else {
                res.render("universities", {universities: []});
            }

        } catch (err) {
            return await next(err);
        }
    },
    async getUniversityById(req, res, next) {
        try {
            const university = await universityRepo.getUniversityById(req.params.id);
            if (university) res.status(200).render('university', {university: university});
            else res.status(404).send({university: null, message: "Not found."});
        } catch (err) {
            return await next(err);
        }
    },
    async deleteUniversityById(req, res, next) {
        try{
            id = req.params.id
            await universityRepo.deleteUniversity(req.params.id);
            res.send({id})
        }catch(err){
            return await next(err);
        }
    },
    async getUsersForNew(req, res, next){
        try{
            const users = await userRepo.getUsers(); 
            if (users) {
                res.status(200).render('newU', {users: users, userDisabled: "disabled"});
            }
            else 
                res.status(404).send({users: null, message: "Not found."});
        }catch(err){
            return await next(err);
        }
    },
    async addUniversity(req, res, next) {
        try {
            console.log(req.files['photoUrl']);
            const photo = await mediaRepo.uploadRaw(req.files['photoUrl'].data);
            const newUniversity = { 
            "name": req.body.name,
            "country": req.body.country,
            "town": req.body.town,
            "photoUrl": photo.url,
            "site": req.body.site,
            "corps": parseInt(req.body.corps),
            "userId": req.body.selectUser};
            console.log(newUniversity);
            const newId = await universityRepo.addUniversity(newUniversity);
            console.log(newId);
            
            newUniversity.id = newId
            newUniversity.add_data = new Date(Date.now())
            newUniversity.url = '/universities/' + newId
            req[universityProperty] = newUniversity
            res.send(req[universityProperty]);
        }catch(err){
            return await next(err);
        }
    },
    async updateUniversity(req, res, next) {
        
    },
    async getAllUniversities(req, res, next) {
        try{
            const universities = await universityRepo.getUniversities();
            if (universities) res.status(200).render('universities', {universities: universities, universityDisabled: "disabled"});
            else res.status(404).send({universities: null, message: "Not found."});
        } catch (err) {
            return await next(err);
        }
    }, 
    async anyError(err, req, res, next){
        console.error(err.stack)
        res.status(500).send('Something broke!')
    }
};