const teacherRepository = require('./../repositories/teacherRepository');
const mediaRepository = require('./../repositories/mediaRepository');
const teacherRepo = new teacherRepository();
const universityRepository = require('./../repositories/universityRepository');
const universityRepo = new universityRepository();
const mediaRepo = new mediaRepository();
notifyAll = require('../bin/www');

const teacherProperty = Symbol("teacher");

module.exports = {
    
    async getTeachers(req, res, next) {
        try {
            console.log(req.headers.onpage)
            if (req.headers.onpage) {
                const teachers = await teacherRepo.getTeacherPaginated(Number(req.query.page), Number(req.query.page_size), req.query.name);
                const pagesNumber = await teacherRepo.getPagesNumber(Number(req.query.page), Number(req.query.page_size), req.query.name);
                //
                let page = req.query.page;
                let name = req.query.name;
                if (!page) page = 1;
                else page = Number(page);
                console.log(req.query.page, req.query.page_size, req.query.name);

                data = {}
                data.teachers = teachers
                data.pagesNumber = pagesNumber
                data.currentPage = page
                req[teacherProperty] = data;
                res.send(req[teacherProperty]);
                
            }else {
                res.render("teachers", {teachers: []});
            }

        } catch (err) {
            return next(err);
        }
    },
    async getTeacherById(req, res, next) {
        try {
            const teacher = await teacherRepo.getTeacherById(req.params.id);
            if (teacher) res.status(200).render('teacher', {teacher: teacher});
            else res.status(404).send({teacher: null, message: "Not found."});
        } catch (err) {
            return next(err);
        }
    },
    async deleteTeacherById(req, res, next) {
        try{
            const id = req.params.id
            await teacherRepo.deleteTeacher(id)
            res.send({id})
        }catch(err){
            return next(err);
        }
    },
    async addTeacher(req, res, next){
        try {
            console.log(req.files['photoUrl']);
            const photo = await mediaRepo.uploadRaw(req.files['photoUrl'].data);
            const newTeacher = { 
            "firstName": req.body.firstName,
            "lastName": req.body.lastName,
            "dataOfBirth": req.body.dataOfBirth,
            "photoUrl": photo.url,
            "email": req.body.email,
            "couplesPerWeek": parseInt(req.body.couplesPerWeek),
            "phone": req.body.phone,
            "universityId": req.body.selectUniversity
             };
            
            const newId = await teacherRepo.addTeacher(newTeacher);
            console.log(newId);
            newTeacher.id = newId;
            newTeacher.add_data = new Date(Date.now());
            newTeacher.url = '/teachers/' + newId;
            newTeacher.name = `${newTeacher.firstName} ${newTeacher.lastName}`
            console.log(newTeacher);
            req[teacherProperty] = newTeacher;
            notifyAll(newTeacher);
            res.send(req[teacherProperty]);
        }catch(err){
            return next(err);
        }
    },
    async getUniversitiesForNew(req, res, next) {
        try {
            const universities = await universityRepo.getUniversities(); 
            if (universities) {
                res.status(200).render('new', {universities: universities, universityDisabled: "disabled"});
            }
            else 
                res.status(404).send({universities: null, message: "Not found."});
        }catch(err){
            return next(err);
        }
    },
    async updateTeacher(req, res, next) {
        try {

            if (!req.body.firstName || !req.body.lastName || !req.body.dataOfBirth || 
                !req.body.photoUrl || !req.body.email || !req.body.phone) 
                res.status(400).send({message: 'Bad request'});
            teacher = await teacherRepo.updateTeacher(req.body);
            if (teacher) {
                console.log(teacher);
                res.status(200).send({teacher: teacher, message: "Success"});
            }
            else 
                res.status(404).send({teacher: null, message: "Not found"});

        } catch (err) {
            return next(err);
        }
    },
    async getAllTeachers(req, res, next) {
        try{
            const teachers = await teacherRepo.getTeachers();
            res.status(200).render('teachers', {teachers: teachers, pagesNumber: 1, pages: 1, teacherDisabled: "disabled"});

        } catch (err) {
            return next(err);
        }

    }, 
    async anyError(err, req, res, next){
        console.error(err.stack)
        res.status(500).send('Something broke!')
    }
};