const userRepository = require('./../repositories/userRepository')
const userRepo = new userRepository();

module.exports = {
    async getUsersPaginated(req, res, next) {
        try {
            users = await userRepo.getUsersPaginated(Number(req.query.page), Number(req.query.per_page));
            if (users) 
                res.status(200).render('users', {users: users, userDisabled: "disabled"});
            else 
                res.status(404).send({users: null, message: "Not found"});
        } catch (err) {
            console.log(err.message);
            res.status(500).send({user: null, message: 'Server error'});
        }
    },
    async getUserById(req, res, next) {
        try {
            const user = await userRepo.getUserById(req.params.id);
            if (user) res.status(200).render('user', {user: user});
            else res.status(404).send({user: null, message: "Not found"});
        } catch(err) {
            console.log(err.message);
            res.status(500).send({user: null, message: 'Server error'});
        }
    },
    async getAllUsers(req, res, next) {
        try {
            users = await userRepo.getUsers();
            if (users) 
                res.status(200).render('users', {users: users, userDisabled: "disabled"});
            else 
                res.status(404).send({users: null, message: "Not found"});
        } catch (err) {
            console.log(err.message);
            res.status(500).send({user: null, message: 'Server error'});
        }
    },
    async anyError(err, req, res, next){
        console.error(err.stack)
        res.status(500).send('Something broke!')
    }
     
};
