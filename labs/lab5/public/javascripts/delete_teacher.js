async function delFunction(){
    try{
        let loading = await fetch("/javascripts/templates/loading_but.mst")
        loading = await loading.text()
        document.getElementById('del').innerHTML=loading
        document.getElementById('del').setAttribute("disabled",true);

        let id = document.getElementById("teacherId").value
        console.log(id)
        let response = await fetch(window.location.pathname, {
            method: "POST",
            params:{
                id: id
            }
        });
        console.log(response);
        response = await response.json()
        console.log(response)
        console.log(response.id)
        if (response.id === id){
            teachers = JSON.parse(localStorage.getItem("teachers"))
            for (let i = 0; i < teachers.lenght; i++){
                if (teachers[i].id === id){
                    teachers.splice(i, 1)
                    localStorage.setItem("teachers", JSON.stringify(teachers));
                }
            }
            let resp2 = await fetch("/javascripts/templates/deleted_teacher.mst")
            resp2 = await resp2.text()
            document.getElementById("del-body").innerHTML = resp2
            $('#deleteModal').modal('hide');
        }else{
            if (response.status === 500) {
                let resp2 = await fetch("/javascripts/templates/errors/error500.mst")
                resp2 = await resp2.text()
                document.getElementById("del-body").innerHTML=resp2
                $('#deleteModal').modal('hide');
            }
            else{
                let resp2 = await fetch("/javascripts/templates/errors/error404.mst")
                resp2 = await resp2.text()
                document.getElementById("del-body").innerHTML=resp2
                $('#deleteModal').modal('hide');
            }
            alert("No, not deleted")
        }
    }catch(error){
        alert(error.message)
    }
};