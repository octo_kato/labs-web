

const protocol = location.protocol === 'https:' ? 'wss:' : 'ws:';
const wsLocation = `${protocol}//${location.host}`;
const connection = new WebSocket(wsLocation);
console.log(wsLocation);
console.log("Hello from web socket");
async function showToastMessage (messageText) {
    try{
        console.log("Show message")
        console.log(messageText)
        let toastEl = await fetch("/javascripts/templates/toast.mst")
        toastEl = await toastEl.text()
        item = messageText
        const renderedHtmlStr = Mustache.render(toastEl, { item});
        const toastOption = {
            animation: true,
            authohide: true,
            delay: 30000
        };
        document.getElementById("cont").innerHTML = 
            document.getElementById("cont").innerHTML + renderedHtmlStr;

        $("#toast" + messageText.id).toast(toastOption);
        $("#toast" + messageText.id).toast("show");
    }catch(err){
        alert(err)
        console.log(err)
    }
}

connection.addEventListener('open', () => console.log('Connected to ws server'));
connection.addEventListener('error', () => console.log('ws error'));
connection.addEventListener('message', async (message) => {
    await showToastMessage(JSON.parse(message.data));
    console.log(`ws message: ${JSON.parse(message.data)}` )
});

connection.addEventListener('close', () => console.log('Disconnected from ws server'));