currentPage = 1;
pagesNumber = 1;
searchStr = "";

window.addEventListener('load', async () => { await renderFunction(); });

document.getElementById('search-teacher').addEventListener("change", async () => {
    currentPage = 1;
    console.log("name")
    searchStr = document.getElementById('search-teacher').value;
    await renderFunction();
});

async function pageFunction(toFetch) {
    currentPage = toFetch;
    await renderFunction();
};

async function prevFunction() {
    if (currentPage > 1) {
        currentPage -= 1;
        await renderFunction();
    }
};

async function nextFunction() {
    if (currentPage < pagesNumber) {
        currentPage += 1;
        await renderFunction();
    }
};

async function renderFunction(){
    try
    {    
        document.getElementById('pag').innerHTML = ""
        document.getElementById('teachers-table').innerHTML = ""
        let inn = document.getElementById('cent').innerHTML
        let element = document.getElementById("search-teacher");
        element.setAttribute("disabled", true);

        let loading = await fetch("/javascripts/templates/loading.mst")
        loading = await loading.text()
        document.getElementById('cent').innerHTML = loading

        let fetchStr = '?page=' + currentPage;
        if (searchStr) fetchStr += "&name=" + searchStr;

        let respData = await fetch("/teachers" + fetchStr, 
        { headers: {
            "OnPage": "true"
        }});
        respData = await respData.json();

        teachers = respData.teachers
        currentPage = respData.currentPage;
        pagesNumber = respData.pagesNumber;

        console.log(currentPage, pagesNumber)
        console.log(teachers)

        pages = []
        for (let i = 1; i <= pagesNumber; i++) {
            page = {};
            page.number = i;
            page.is_active = "";
            pages.push(page);
        }
        pages[currentPage - 1].is_active = "active"
        prev_dis = ""
        next_dis = ""

        if (pages[0].is_active === "active") prev_dis = "disabled";
        if (pages[pagesNumber - 1].is_active === "active") next_dis = "disabled";

        let pagination = await fetch("/javascripts/templates/pagination.mst")
        pagination = await pagination.text()

        document.getElementById('cent').innerHTML = inn;
        const renderedHtmlStrPag = Mustache.render(pagination, {pages, prev_dis, next_dis});
        document.getElementById('pag').innerHTML = renderedHtmlStrPag

        let resp = await fetch("/javascripts/templates/teachers_table.mst")
        console.log(resp)
        resp = await resp.text();
        console.log(resp)


        const renderedHtmlStr = Mustache.render(resp, {teachers});
        console.log(renderedHtmlStr)
        const appEl = document.getElementById('teachers-table');
        appEl.innerHTML = renderedHtmlStr;
        element.removeAttribute("disabled");

    }catch(err){
        console.log(err)
        alert(err)
    }
}