const readline = require('readline-sync');
const UserRepository = require('./repositories/userRepository');
const userRepository = new UserRepository('./data/users.json');

const TeacherRepository = require('./repositories/teacherRepository');
const teacherRepository = new TeacherRepository('./data/teachers.json');

const Teacher = require('./models/teacher');

while (true) {
    const input = readline.question("Enter command:");
    const arrayOfStrings = input.split('/');
    const len = arrayOfStrings.length;
    console.log('The command consists of ' + len + ' parts: ' + arrayOfStrings.join('--'));
    var entityId = 0;

    if(len == 3){ //take id if len == 3
        const arr = arrayOfStrings[2].split('');
        if(!arr.every(x => (x >= '0' && x <= '9'))){
            console.log('incorrectly set ID');
            continue;
        }
        if(arr == ""){
            console.log('ID is not set');
            continue;
        }
        entityId = Number(arrayOfStrings[2]);
    }
    if(len == 2 || len == 3 ){
        // teachers case
        if(arrayOfStrings[1] == "teachers"){ 
            switch(arrayOfStrings[0]){
                case 'get':{
                    if(len == 2){
                        const ts = teacherRepository.getTeachers();
                        separator();
                        for (const t of ts) {
                            printT(t);
                            separator();
                        }
                    }else{
                        const t = teacherRepository.getTeacherById(entityId);
                        if (!t) {
                            console.log(`teacher with id ${entityId} not found.`);
                        }
                        else {
                            separator();
                            printT(t);
                            console.log(`Birthday: ${t.dataOfBirth}\nPhoto  URL: ${t.photoUrl}\nEmail: ${t.email}\nNumber of couples per week: ${t.couplesPerWeek}\nPhone: ${t.phone}`);
                            separator();
                        }
                    }
                
                }
                break;
                case 'delete':{
                    if (teacherRepository.deleteTeacher(entityId))
                        console.log("deleted succesfully");
                    else console.log(`teacher with id ${entityId} not found.`);
                }
                break;
                case 'update':{
                    const teacher = teacherRepository.getTeacherById(entityId);
                    if (!teacher) {
                        console.log(`teacher with id ${entityId} not found.`);
                        break;
                    }
                    let cond = true;
                    while(cond){
                        separator();
                        printT(teacher);
                        console.log(`Birthday: ${teacher.dataOfBirth}
Photo  URL: ${teacher.photoUrl}
Email: ${teacher.email}
Number of couples per week: ${teacher.couplesPerWeek}
Phone: ${teacher.phone}`);
                        separator();
                        console.log('Choose what you want to update:\n',
                            '1 - First name\n',
                            '2 - Last name\n',
                            '3 - Birthday\n',
                            '4 - Photo  URL\n',
                            '5 - Email\n',
                            '6 - Number of couples per week\n',
                            '7 - Phone\n',
                            'something else - Quit');
                        const input1 = readline.question(">  ");
                        switch(input1){
                            case "1":
                                const newfn = readline.question("Enter new first name:");
                                teacher.firstName = newfn;
                            break;
                            case "2":
                                const newln = readline.question("Enter new last name:");
                                teacher.lastName = newln;
                            break;
                            case "3":
                                const newd = readline.question("Enter new day:");
                                try {
                                    const day = new Date(newd);
                                    teacher.dataOfBirth = day.toISOString();
                                }
                                catch (err) {
                                    console.log('incorrect Date format');
                                    console.log('nothing changed...');
                                    break;
                                }
                            break;
                            case "4":
                                const newpu = readline.question("Enter new photo URL:");
                                teacher.photoUrl = newpu;
                            break;
                            case "5":
                                const newe = readline.question("Enter new email:");
                                teacher.email = newe;
                            break;
                            case "6":
                                const newc = Number(readline.question("Enter new number of couples:"));
                                if(!isNaN(newc) && newc >= 0){
                                    teacher.couplesPerWeek = newc;
                                }else{
                                    console.log("incorrect value");
                                    break;
                                }
                            break;
                            case "7":
                                const newp = readline.question("Enter new phone:");
                                teacher.phone = newp;
                            break;
                            default:{
                                console.log("Your choice - Quit");
                                cond = false;
                                break;   
                            }
                        }
                        if(cond & teacherRepository.updateTeacher(teacher)){
                            console.log("the update was succeful");
                        }else if(cond) console.log("update failed");
                    } 
                }
                break;
                case 'post':{
                    const teacher = new Teacher({"id": 0,
                    "firstName": " ",
                    "lastName": " ",
                    "dataOfBirth": " ",
                    "photoUrl": " ",
                    "email": " ",
                    "couplesPerWeek": 10,
                    "phone": " "});
                    //teacher.id = parseInt('1');
                    while(1){
                        const newnt = readline.question("Enter first name:");
                        if(newnt.length !== 0){
                            teacher.firstName = parseInt(newnt);
                            break;
                        }
                        console.log('try again');
                    }while(1){
                        const newlnt = readline.question("Enter last name:");
                        if(newlnt.length !== 0){
                            teacher.lastName = newlnt;
                            break;
                        }
                        console.log('try again');
                    }while(1){
                        const newbt = readline.question("Enter birthday:");
                        try {
                            const day = new Date(newbt);
                            console.log(`Day: ${day}`);
                            teacher.dataOfBirth = day.toISOString();
                            console.log(teacher.dataOfBirth);
                        }
                        catch (err) {
                            console.log('incorrect Date format');
                            continue;
                        }
                        break;
                    }while(1){
                        const newut = readline.question("Enter photo URL:");
                        if(newut.length !== 0){
                            teacher.photoUrl = newut;
                            break;
                        }
                        console.log('try again');
                    }while(1){
                        const newet = readline.question("Enter email:");
                        if(newet.length !== 0){
                            teacher.email = newet;
                            break;
                        }
                        console.log('try again');
                    }while(1){
                        const newnct = readline.question("Enter number of couples:");
                        const newnc =  Number(newnct);
                        if(!isNaN(newnc) && newnc >= 0){
                            teacher.couplesPerWeek = newnc;
                            break;
                        }else console.log("incorrect value");
                    }while(1){
                        const newpt = readline.question("Enter phone:");
                        if(newpt.length !== 0){
                            teacher.phone = newpt;
                            break;
                        }
                        console.log('try again');
                    }
                    separator();
                    printT(teacher);
                    console.log(`Birthday: ${teacher.dataOfBirth}
Photo  URL: ${teacher.photoUrl}
Email: ${teacher.email}
Number of couples per week: ${teacher.couplesPerWeek}
Phone: ${teacher.phone}`);
                    separator();
                    let id = teacherRepository.addTeacher(teacher);
                    console.log(`new id - ${id}`);
                }
                break;
                default:{
                    console.log('wrong first part of the command');
                }
            }
        }
        // user case
        else if(arrayOfStrings[1] == "users" && arrayOfStrings[0] == "get"){
            switch(len){
                case 2:{
                    const users = userRepository.getUsers();
                    separator();
                    for (const user of users) {
                        printU(user);
                        separator();
                    }
                }
                break;
                case 3:{
                    const user = userRepository.getUserById(entityId);
                    if (!user) {
                        console.log(`user with id ${entityId} not found.`);
                    }
                    else {
                        separator();
                        printU(user);
                        console.log(`Role: ${user.role}\nAvatar url: ${user.avaUrl}\nIs enabled: ${user.isEnebled}`);
                        separator();
                    }
                }
                break;
                default:{
                    console.log('incorrect command...............?');
                }
            }  
        }else console.log('incorrect command');
    }else if(input == 'help'){
        separator();
        console.log('List of command:\n',
        'get/users\n',
        'get/users/{id}\n',
        'get/teachers\n',
        'get/teachers/{id}\n',
        'delete/teachers/{id}\n',
        'update/teachers/{id}\n',
        'post/teachers\n',
        'quit');
    }else if(input == 'quit') break;
    else{
        console.log('incorrect command')
    }
}

function printT(ts){
    console.log(`Id: ${ts.id}\nFirst name: ${ts.firstName}\nLast name: ${ts.lastName}\n`)
}

function printU(us){
    console.log(`Id: ${us.id}\nLogin:${us.login}\nName: ${us.fullname}\n`);
}

function separator(){
    console.log('-------------------------------------------------------\n');
}