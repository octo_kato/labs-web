const Teacher = require('../models/teacher');
const JsonStorage = require('../jsonStorage');
 
class TeacherRepository {
 
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }
 
    getTeachers() { 
        const items = this.storage.readItems();
        const teachers = [];
        for (const item of items) {
            teachers.push(new Teacher(item));
        }
        return teachers;
    }

    addTeacher(teacherModel){
        const teachers = this.getTeachers();
        teacherModel.id = this.storage.nextId;
        teachers.push(teacherModel);
        this.storage.incrementNextId();
        this.storage.writeItems(teachers);
        return teacherModel.id;
    }

    getTeachers(){
        const items = this.storage.readItems();
        const teachers = [];
        for (const item of items) {
            teachers.push(new Teacher(item));
        }
        return teachers;
    }

    getTeacherById(teacherId) {
        const items = this.storage.readItems();
        for (const item of items) {
            if (item.id === teacherId) {
                return new Teacher(item);
            }
        }
        return null;
    }

    updateTeacher(teacherModel){
        const teachers = this.getTeachers();
        const teacher = this.getTeacherById(teacherModel.id);
        if (teacher !== null) {
            const index = teachers.findIndex((t) => {
                return t.id === teacher.id;
            });
            teachers.splice(index, 1, teacherModel);
            this.storage.writeItems(teachers);
            return true;
        }
        else return false;
    }

    deleteTeacher(teacherId){
        const teachers = this.getTeachers();
        const teacher = this.getTeacherById(teacherId);
        if (teacher !== null) {
            const index = teachers.findIndex((t) => {
                return t.id === teacher.id;
            });
            teachers.splice(index, 1);
            this.storage.writeItems(teachers);
            return true;
        }
        else return false;
    }
};
 
module.exports = TeacherRepository;
