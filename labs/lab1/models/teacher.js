class Teacher {
    constructor(model) {
        this.id = model.id;
        this.firstName = model.firstName;
        this.lastName = model.lastName;
        this.dataOfBirth = model.dataOfBirth;
        this.photoUrl = model.photoUrl;
        this.email = model.email;
        this.couplesPerWeek = model.couplesPerWeek;
        this.phone = model.phone; 
    }
};
 
module.exports = Teacher;