class User {

    constructor(model) {
        this.id = model.id;  
        this.login = model.login; 
        this.fullname = model.fullname; 
        this.role = model.role;
        this.avaUrl = model.avaUrl;
        this.isEnebled = model.isEnebled;
    }
 };
 
 module.exports = User;
 