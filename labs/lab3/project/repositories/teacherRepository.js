const Teacher = require('../models/teacher');
const JsonStorage = require('./jsonStorage');
 
class TeacherRepository {
 
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }
    addTeacher(teacherModel){
        const teachers = this.storage.readItems().items;
        teacherModel.id = this.storage.nextId;
        teachers.push(teacherModel);
        this.storage.incrementNextId();
        this.storage.writeItems(teachers);
        return teacherModel.id;
    }
    getTeachers(){
        return this.storage.readItems().items;
    }
    getTeacherById(teacherId) {
        const items = this.storage.readItems().items;
        for (const item of items) {
            if (item.id ===  parseInt(teacherId)) {
                return new Teacher(item);
            }
        }
        return null;
    }
    getPagesNumber(page, per_page, name) {
        const page_size = 3;
        const maxPageSize = 3;
        if (per_page) {
            if (per_page > maxPageSize) {
                console.log("Error");
                return undefined;
            }
        }
        else per_page = page_size;

        if (!page) page = 1;

        const teachers = this.getTeachers();
        const teachersNumber = Number(teachers.length)
        const offset = per_page * (page - 1);

        if (teachersNumber <= offset) {
            console.log("Error");
            return undefined;
        }

        let resTeachers = [];
        let tempTeachersLen = 0;

        if (name) {
            for (let i = 0; i < teachers.length; i++) {
                if (teachers[i].lastName.includes(name)) resTeachers.push(teachers[i]);
            }
            tempTeachersLen = resTeachers.length;
            resTeachers = resTeachers.slice(offset, offset + per_page);
        }
        const currentTeachers = teachers.slice(offset, offset + per_page);
        let pagesNumber = 0;

        if ((teachersNumber / per_page) - Math.trunc(teachersNumber / per_page) != 0) 
            pagesNumber = Math.trunc(teachersNumber / per_page) + 1;
        else
            pagesNumber = Math.trunc(teachersNumber / per_page);

        if (name) {
            if ((tempTeachersLen / per_page) - Math.trunc(tempTeachersLen / per_page) != 0) 
                pagesNumber = Math.trunc(tempTeachersLen / per_page) + 1;
            else 
                pagesNumber = Math.trunc(tempTeachersLen / per_page);

            if (pagesNumber == 0) pagesNumber = 1;

            return pagesNumber;
        }

        if (pagesNumber == 0) pagesNumber = 1;
        
        return pagesNumber;
    }

    getTeacherPaginated(page, per_page, name) {
        const page_size = 3;
        const maxPageSize = 3;
        if (per_page) {
            if (per_page > maxPageSize) {
                console.log("Error");
                return undefined;
            }
        }
        else per_page = page_size;

        if (!page) page = 1;

        const teachers = this.getTeachers();
        const teachersNumber = Number(teachers.length);
        const offset = per_page * (page - 1);

        if (teachersNumber <= offset) {
            console.log("Error");
            return undefined;
        }

        let resTeachers = [];

        if (name) {
            for (let i = 0; i < teachers.length; i++) {
                if (teachers[i].lastName.includes(name)) 
                    resTeachers.push(teachers[i]);
            }
            resTeachers = resTeachers.slice(offset, offset + per_page);
        }
        const currentTeachers = teachers.slice(offset, offset + per_page);
        if (name) return resTeachers;
        return currentTeachers;
    }
    updateTeacher(teacherModel){
        const teachers = this.storage.readItems().items;

        if (teacherModel.id < this.storage.readItems().nextId) {
            let check = false;
            let index = 0;
            console.log(teacherModel.id);

            for (const item of teachers) {
                if (parseInt(item.id) === parseInt(teacherModel.id)) {
                    teacherModel.id = parseInt(teacherModel.id);
                    teacherModel.couplesPerWeek = parseInt(teacherModel.couplesPerWeek);

                    console.log("Teacher has been updated successfully.");
                    check = true;
                    const updatedTeacher = new Teacher(teacherModel);
                    teachers[index] = updatedTeacher;
                    break;
                }
                index++;
                console.log(index);
            }
            if (check === false) console.log("Teacher with such id doesn't exist.");
        }
        else console.log("Teacher with such id doesn't exist");
        this.storage.writeItems(teachers);
        return teacherModel;
    }
    deleteTeacher(teacherId){
        const teachers = this.storage.readItems().items;
        const teacher = this.getTeacherById(teacherId);
        if (teacher) {
            const index = teachers.findIndex((t) => {
                return t.id === teacher.id;
            });
            teachers.splice(index, 1);
            this.storage.writeItems(teachers);
            console.log("Teacher has been deleted successfully.");
            return teacher;
        }
        else console.log("Teacher with such id doesn't exist");
        return undefined;
    }
};
 
module.exports = TeacherRepository;
