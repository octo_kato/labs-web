const fs = require("fs");
class JsonStorage {

    // filePath - path to JSON file
    constructor(filePath) {
        this._filePath = filePath;
    }

    get nextId() {
        // TODO: get next entity id
        return (JSON.parse(fs.readFileSync(this._filePath))).nextId;
    }

    incrementNextId() {
        // TODO: increment next entity id 
        const file = JSON.parse(fs.readFileSync(this._filePath));
        file.nextId++;
        fs.writeFileSync(this._filePath, JSON.stringify(file, null, 4, (err) => {
            if (err) throw err;
        }));
    }

    readItems() {
        // TODO: return all items from JSON file
        return JSON.parse(fs.readFileSync(this._filePath));
    }

    writeItems(items) {
        // TODO: write all items to JSON file
        const file = JSON.parse(fs.readFileSync(this._filePath));
        file.items = items;
        fs.writeFileSync(this._filePath, JSON.stringify(file, null, 4), (err) => {
            if(err) throw err;
            console.log(err);
        });
    }
};

module.exports = JsonStorage;