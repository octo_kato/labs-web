const JsonStorage = require('./jsonStorage');
const fs = require('fs');

class MediaRepository {
 
    constructor(filePath) {
        this.path = (filePath.split('.'))[0];
        this.storage = new JsonStorage(filePath);
    }
    getMediaPath(id) {
        for (const item of this.allFileFormats()) {
            const fullPath = this.path + '/teacher' + String(id) + '.' + item;
            if (fs.existsSync(fullPath)) return fullPath;
        }
        return undefined;
    }
    getCurrentId() {
        return this.storage.nextId - 1;
    }
    allFileFormats() {
        return this.storage.readItems().fileFormats;
    }
};
 
module.exports = MediaRepository;