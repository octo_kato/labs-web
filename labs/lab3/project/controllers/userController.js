const userRepository = require('./../repositories/userRepository')
const userRepo = new userRepository('./data/users.json');

module.exports = {
    getUsersPaginated(req, res) {
        try {
            users = userRepo.getUsersPaginated(Number(req.query.page), Number(req.query.per_page));
            if (users) 
                res.status(200).render('users', {users: users, userDisabled: "disabled"});
            else 
                res.status(404).send({users: null, message: "Not found"});
        } catch (err) {
            console.log(err.message);
            res.status(500).send({user: null, message: 'Server error'});
        }
    },
    getUserById(req, res) {
        try {
            const user = userRepo.getUserById(parseInt(req.params.id));
            if (user) res.status(200).render('user', {user: user});
            else res.status(404).send({user: null, message: "Not found"});
        } catch(err) {
            console.log(err.message);
            res.status(500).send({user: null, message: 'Server error'});
        }
    },
    getAllUsers(req, res) {
        try {
            users = userRepo.getUsers();
            if (users) 
                res.status(200).render('users', {users: users, userDisabled: "disabled"});
            else 
                res.status(404).send({users: null, message: "Not found"});
        } catch (err) {
            console.log(err.message);
            res.status(500).send({user: null, message: 'Server error'});
        }
    },
};
