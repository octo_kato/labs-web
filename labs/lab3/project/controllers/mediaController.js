const mediaRepositpory = require('./../repositories/mediaRepository');
const multer = require('multer');
const path = require('path');
const mediaRepo = new mediaRepositpory('./data/media.json');
const fs = require('fs');

const upload = multer({
    storage: multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, mediaRepo.path);
        },
        filename: (req, file, cb) => {
            const fileFormat = file.mimetype.split('/')[1];
            cb(null, `${String(mediaRepo.getNextId())}.${fileFormat}`);
        },
    }),

}).any()

module.exports = {
    getMediaById(req, res) {
        try {
            const mediaId = Number(req.params.id);
            const path = mediaRepo.getMediaPath(mediaId);

            if (path) res.sendFile(path);
            else res.status(404).send({media: null, message: 'Not found'});
        } catch (err) {
            console.log(err.message);
            res.status(500).send({media: null, message: 'Server error'});
        }
    },
    addMedia(req, res) {
        try {
            upload(req, res, (err) => {
                if (err) {
                    console.log('err ', err.message);
                    res.status(500).send({media: null, message: 'Server error'});
                    return 0;
                } else if (req.files) {
                    const fileFormat = req.files[0].mimetype.split('/')[1];
                    if (!mediaRepo.allFileFormats().includes(fileFormat)) {
                        res.status(400).send({message: 'Bad request'});
                    }
                    else {
                        res.status(201).send({mediaId: mediaRepo.storage.nextId, message: 'Media has been uploaded'});
                        mediaRepo.incrementId();
                    }
                } else res.status(400).send({message: 'Bad request'});
            });
        } catch (err) {
            console.log(err.message);
            res.status(500).send({media: null, message: 'Server error'});
        }
    }
};
