const Router = require('express').Router();
const teacherController = require('./../controllers/teacherController');

Router.get("/new", (req, res) => { res.status(200).render('new') });
Router.get("/:id", teacherController.getTeacherById);
Router.post("/:id", teacherController.deleteTeacherById);
Router.get("/", teacherController.getTeachers);
Router.post("/", teacherController.addTeacher);

module.exports = Router;