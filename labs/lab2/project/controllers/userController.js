const userRepositpory = require('./../repositories/userRepository')
const userProperty = Symbol('user')
const userRepo = new userRepositpory('./data/users.json');

module.exports = {
    getUsers(req, res) {
        const page = req.query.page;
        const per_page = req.query.per_page;
        if (page && per_page) {
            req[userProperty] = userRepo.getUsers().slice((page - 1) * per_page, page * per_page);
        }
        else {
            req[userProperty] = userRepo.getUsers();
        } 
        res.send(req[userProperty]);
    },
    getUserById(req, res) {
        const user = userRepo.getUserById(parseInt(req.params.id));
        if (user) {
            req[userProperty] = user;
            res.send(req[userProperty]);
        } 
        else res.sendStatus(404);
    },
};
