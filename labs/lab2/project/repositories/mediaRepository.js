const Media = require('../models/media');
const JsonStorage = require('./jsonStorage');
 
class MediaRepository {
 
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }
    getMediaById(id) {
        const items = this.storage.readItems();
        for (const item of items) {
            if (item.id === id) {
                return new Media(item.id, item.filename);
            }
        }
        return null;
    }
    addMedia(media){
        let items = this.storage.readItems();
        const medias = [];
        for (const item of items) {
            medias.push(new Media(item.id, item.filename));
        }
        media.id = this.storage.nextId;
        medias.push(media);
        this.storage.incrementNextId();
        this.storage.writeItems(medias);
        return media.id;
    }
};
 
module.exports = MediaRepository;