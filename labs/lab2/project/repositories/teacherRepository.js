const Teacher = require('../models/teacher');
const JsonStorage = require('./jsonStorage');
 
class TeacherRepository {
 
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }
 
    getTeachers() { 
        const items = this.storage.readItems();
        const teachers = [];
        for (const item of items) {
            teachers.push(new Teacher(item));
        }
        return teachers;
    }

    addTeacher(teacherModel){
        const teachers = this.getTeachers();
        teacherModel.id = this.storage.nextId;
        teachers.push(teacherModel);
        this.storage.incrementNextId();
        this.storage.writeItems(teachers);
        return teacherModel.id;
    }

    getTeachers(){
        const items = this.storage.readItems();
        const teachers = [];
        for (const item of items) {
            teachers.push(new Teacher(item));
        }
        return teachers;
    }

    getTeacherById(teacherId) {
        const items = this.storage.readItems();
        for (const item of items) {
            if (item.id ===  parseInt(teacherId)) {
                return new Teacher(item);
            }
        }
        return null;
    }

    updateTeacher(teacherModel){
        const items = this.storage.readItems();
        for (let i=0; i<items.length; i++) {
            if (items[i].id ===  teacherModel.id){
                item = teacherModel;
                this.storage.writeItems(items);
                return true;
            }
        }
        return false;
    }

    deleteTeacher(teacherId){
        const teachers = this.storage.readItems();
        const teacher = this.getTeacherById(teacherId);
        if (teacher !== null) {
            const index = teachers.findIndex((t) => {
                return t.id === teacher.id;
            });
            teachers.splice(index, 1);
            this.storage.writeItems(teachers);
            return true;
        }
        else return false;
    }
};
 
module.exports = TeacherRepository;
