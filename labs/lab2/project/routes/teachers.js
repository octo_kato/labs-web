const express = require('express');
const Teacher = require('../models/teacher');
const teacherController = require('./../controllers/teacherController');

const Router = express.Router();
/**
 * returns all teachers
 * @route GET /api/teachers/
 * @group Teachers - teacher operations
 * @param {integer} page.query - page number
 * @param {integer} per_page.query - items per page
 * @returns {Array.<Teacher>} Teachers - all teachers
 */
Router.get('/', teacherController.getTeachers)
/**
* return teacher by id
* @route GET /api/teachers/{id}
* @group Teachers - teacher operations
* @param {integer} id.path.required - id of the Teacher - eg: 1
* @returns {Teacher.model} 200 - Teacher object
* @returns {Error} 404 - Teacher not found
*/
    .get('/:id', teacherController.getTeacherById)
/**
* add teacher
* @route POST /api/teachers/
* @group Teachers - teacher operations
* @param {file} teacher.formData.required - new Teacher object
* @returns {Teacher.model} 201 - added Teacher object
* @returns {Error} 400 - Bad request
*/
    .post('/', teacherController.addTeacher)
/**
* update teacher
* @route PUT /api/teachers/
* @group Teachers - teacher operations
* @param {integer} id.path.required - new Teacher object
* @param {file} teacher.formdData.required - new Teacher object
* @returns {Teacher.model} 200 - changed Teacher object
* @returns {Error} 404 - Cat not found
*/
    .put('/:id(\\d+)',teacherController.getTeacherHandler, teacherController.updateTeacher)
/**
* delete teacher
* @route DELETE /api/teachers/{id}
* @group Teachers - teacher operations
* @param {integer} id.path.required - id of the Teacher - eg: 1
* @returns {Teacher.model} 200 - deleted Teacher object
* @returns {Error} 404 - Teacher not found
*/
    .delete('/:id', teacherController.getTeacherHandler, teacherController.deleteTeacherById);
    
module.exports = Router;